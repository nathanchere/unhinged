from unhinged.archive_handler import ArchiveHandler
from unhinged.model.hinge_data import HingeData

def test_integration_of_archive_loading():
    input_path = 'tests/data/valid_detailed.7z'
    archive = ArchiveHandler(input_path)
    data = HingeData.from_json_string(archive.matches_json)

    assert len(data.root) == 9

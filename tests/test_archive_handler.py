import pytest
from unhinged.archive_handler import *

# Error conditions

def test_raises_error_on_file_not_exists():
    # The specified file does not exist
    with pytest.raises(FileNotFoundError):
        ArchiveHandler('tests/data/nonexistent_archive.7z')

def test_raises_error_on_invalid_archive():
    # The file exists but is not a valid 7zip archive
    with pytest.raises(Bad7zFile):
        ArchiveHandler('tests/data/invalid_archive.7z')

def test_raises_error_on_archive_missing_expected_files():
    # Is a valid 7zip archive but is missing required files
    with pytest.raises(InvalidArchiveContentsException):
        # TODO: would be nice if it also says which expected files are missing
        ArchiveHandler('tests/data/missing_files.7z')

def test_raises_error_on_invalid_file_content():
    # The expected files exist in the archive but have invalid content    
    with pytest.raises(JSONDecodeError):
        ArchiveHandler('tests/data/invalid_content.7z')

# Success conditions
        
def test_extracts_correct_data_from_valid_archive():
    expected = "[{}]\n"
    handler = ArchiveHandler('tests/data/valid_minimal.7z')
    assert expected == handler.matches_json
import pytest
from unhinged.filters import *
from unhinged.model.hinge_data import HingeData

@pytest.fixture
def connections():
    file_path = 'tests/data/example_matches.json'
    with open(file_path, 'r') as file:
        return HingeData.from_json_string(file.read()).root
    
def test_has_match(connections):
    result = apply_filter(connections, "has_match", has_match)
    assert len(result) == 8

def test_has_chats(connections):
    result = apply_filter(connections, "has_chats", has_chats)
    assert len(result) == 3

def test_has_no_block(connections):
    result = apply_filter(connections, "has_no_block", has_no_block)
    assert len(result) == 6

def test_has_chat_with_text(connections):
    result = apply_filter(connections, "has_chat_with_text", has_chat_with_text, "bacon")
    assert len(result) == 1

import pytest
from unhinged.model.hinge_data import HingeData

@pytest.fixture
def json_data():
    with open('tests/data/example_matches.json') as file:
        return file.read()

def test_can_parse_json_to_model(json_data):
    HingeData.from_json_string(json_data)

def test_has_expected_number_of_connections(json_data):
    sut = HingeData.from_json_string(json_data)
    assert len(sut.root) == 9

def test_parses_expected_like_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[0].like
    assert len(sut) == 1
    assert sut[0].timestamp == "2020-02-06 12:34:56"

def test_parses_expected_unmatch_block_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[0].block
    assert len(sut) == 1
    assert sut[0].block_type == "remove"

def test_parses_expected_report_block_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[3]
    assert len(sut.block) == 1
    assert sut.block[0].block_type == "report"

def test_parses_expected_match_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[0].match
    assert len(sut) == 1

def test_parses_expected_chat_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[0].chats
    assert len(sut) == 3
    assert sut[2].body == "Did you just insult my mother?"

def test_parses_expected_we_met_items(json_data):
    model = HingeData.from_json_string(json_data)
    sut = model.root[1].we_met
    assert len(sut) == 1
    assert sut[0].was_my_type == True

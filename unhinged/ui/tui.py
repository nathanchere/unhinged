from prompt_toolkit.shortcuts import radiolist_dialog, button_dialog, message_dialog
from prompt_toolkit.styles import Style
from prompt_toolkit import prompt
from ..common import *

prompt_select_mode = """Do you want to:
            
* Extract a specific match from your Hinge data export?
* Merge two match extracts into a final archive?"""

style = Style.from_dict(
    {
        "dialog": "bg:#ffffff",
        "frame.label": "bg:#ffffff #000000",
        "frame": "bg:#000000 #ffffff",
        "dialog.body": "bg:#000000 #ffffff",
        "dialog shadow": "bg:#444444",
        "button": "fg:#ffffff",
        "button.focused": "bg:#ffff00 fg:#000000",
        "button.arrow": "bg:#000000 fg:#ffffff",
        "radio-checked": "bg:#ffff00 fg:#000000",
        "radio-selected": "bg:#ffff00 fg:#000000",
    }
)

error_style = Style.from_dict(
    {
        "dialog": "bg:#660000",
        "frame.label": "bg:#660000 #000000",
        "frame": "bg:#000000 #ffffff",
        "dialog.body": "bg:#000000 #ffffff",
        "dialog shadow": "bg:#444444",
        "button": "fg:#ffffff",
        "button.focused": "bg:#ffff00 fg:#000000",
        "button.arrow": "bg:#000000 fg:#ffffff",
    }
)

class Tui:
    def select_mode(self):
        return button_dialog(
            title=APP_TITLE,
            text=prompt_select_mode,
            style=style,
            buttons=[
                ('Extract', MODE_EXTRACT),
                ('Merge', MODE_MERGE),
                ('Quit', MODE_QUIT)
            ],
        ).run()

    def select_file(self, prompt, files):
        self.selected_file = radiolist_dialog(
            title=APP_TITLE,
            text=prompt,
            style=style,
            values=[(f, f) for f in reversed(files)],
        ).run()

        return self.selected_file

    def prompt_input(self, text):
        return prompt(text)

    def alert(self, text):
        message_dialog(
            title=APP_TITLE,
            style=style,
            text=text).run()

    def alert_error(self, text):
        message_dialog(
            title=APP_TITLE,
            style=error_style,
            text=f'Oh no - something went wrong :(\n\n{text}').run()

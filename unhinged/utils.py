import pathlib
import re

def find_unique_type_nodes(input_json: str) -> list[str]:
    """
    Find and return a list of unique "type" nodes from the input JSON.

    Pass in the contents of a "matches.json" file from a Hinge data export.
    """

    pattern = r'"type":\s*"([^"]+)"'
    matches = re.findall(pattern, input_json)
    return list(set(matches))

def get_data_path() -> str:
    """
    Helper for reliably fetching the ./data folder in the repository root regardless of running native or virtual env
    """
    return str(pathlib.Path(__file__).parent.parent.joinpath('data').resolve())

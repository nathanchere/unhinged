from .model.hinge_data import *

def apply_filter(contacts: list[ConnectionItem], description, filter_func, *args):
    print(f"Applying filter: {description}")
    before = len(contacts)
    result = list(filter(lambda x: filter_func(x, *args), contacts))
    after = len(result)
    print(f" -> {after} connections remaining (removed {before - after})")
    return result

def has_match(connection: ConnectionItem):
    return connection.match

def has_chats(connection: ConnectionItem):
    return connection.chats

def has_no_block(connection: ConnectionItem):
    return not connection.block

def has_chat_with_text(connection: ConnectionItem, text:str):
    text_lower = text.lower()
    for chat in connection.chats:
        if text_lower in chat.body.lower():
            return True
    return False


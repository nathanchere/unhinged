from .extract_merger import ExtractMerger
from .model.hinge_data import HingeData
from .archive_handler import *
from .ui.tui import *
from .utils import *
from .filters import *
from .common import *
from glob import glob
from os import path

tui = Tui()

def save_extracted_data(connection_item):
    name = tui.prompt_input("Enter your name (first name, nickname, whatever):")
    file_name = path.join(get_data_path(),f"HingeExtract.{name}.json")
    json_data = connection_item.model_dump_json()
    with open(file_name, "w") as file:
        file.write(json_data)
    return file_name

def do_extract():
    data_path = get_data_path()
    file_pattern = path.join(data_path, "*.7z")
    all_files = glob(file_pattern)
    selected_file = tui.select_file(f"Select your Hinge data archive\n(looking in {data_path}):", all_files)
    if not selected_file:
        return
    archive = ArchiveHandler(selected_file)
    contacts = HingeData.from_json_string(archive.matches_json).root

    count_initial = len(contacts)

    print(f'Initial contacts count: {count_initial}')
    contacts = apply_filter(contacts, "reciprocal match", has_match)
    contacts = apply_filter(contacts, "has chats", has_chats)
    contacts = apply_filter(contacts, "not blocked", has_no_block)

    query=1
    while len(contacts) > 1:
        query=tui.prompt_input("Enter a filter query (or leave blank to stop): ")
        if not query:
            return   

        new_contacts = apply_filter(contacts, f"contacts '{query} in chat'", has_chat_with_text, query)
        if not new_contacts:
            print(f"Oops - removed all connections! Undoing the last filter for '{query}'")        
        else:
            contacts = new_contacts
    print("\n================================================")
    print("========------[ 1 MATCH FOUND! ]-----===========")
    print("================================================\n")
    output_file = save_extracted_data(contacts[0])
    tui.alert(f"Extracted data saved to {output_file}")

def do_merge():
    data_path = get_data_path()
    file_pattern = path.join(data_path, "HingeExtract.*.json")
    all_files = glob(file_pattern)
    if len(all_files) < 2:
        tui.alert_error(f"You need at least two HingeExtract files to merge\nMake sure they are saved in {data_path}")
        return
    
    file_a = tui.select_file(f"Select the first data extract to merge\n(looking in {data_path}):", all_files)
    if not file_a:
        return
    all_files.remove(file_a)

    file_b = tui.select_file(f"Select the second data extract to merge\n(looking in {data_path}):", all_files)
    if not file_b:
        return

    merged = ExtractMerger(file_a, file_b).merge()
    output_file = path.join(data_path, f'Hinge.{merged.users[0].name} and {merged.users[1].name}.json')
    with open(output_file, 'w') as file:
        file.write(merged.model_dump_json())
    tui.alert(f"Merge data saved to {output_file}\nTotal messages: {len(merged.chats)}")

def main():
    mode = ''
    while mode != MODE_QUIT:
        try:
            mode = tui.select_mode()
            if mode == 'extract':
                do_extract()
            elif mode == 'merge':
                do_merge() 
        except Exception as e:
            tui.alert_error(e)
            exit(1)

if __name__ == "__main__":
    main()

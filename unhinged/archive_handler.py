from py7zr import SevenZipFile, Bad7zFile
from json.decoder import JSONDecodeError
import json

class InvalidArchiveException(Exception):
    "When the input file is not a valid 7zip archive"
    pass

class InvalidArchiveContentsException(Exception):
    "When the input file is missing contents expected in a Hinge data export"

    def __init__(self, message):
        super().__init__(message)


MATCHES_JSON = 'matches.json'

class ArchiveHandler:
    matches_json: str = None
    
    def __init__(self, archive_path):
        self.matches_json
        required_files = {MATCHES_JSON}

        with SevenZipFile(archive_path, mode='r') as z:
            filenames = set(z.getnames())
            if not required_files.issubset(filenames):
                raise InvalidArchiveContentsException("Archive is missing required files")
            
            raw = z.read(MATCHES_JSON)[MATCHES_JSON].read().decode('utf-8')
            json.loads(raw) # kind of redundant, just here to raise error early if not valid JSON
            self.matches_json = raw

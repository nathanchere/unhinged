from unhinged.model.hinge_extract import *
import re

class ExtractMerger:
    extract_a: HingeExtract 
    extract_b: HingeExtract 
    
    def __init__(self, extract_a_path, extract_b_path):
        self.extract_a = self._load_hinge_extract(extract_a_path, 1)
        self.extract_b = self._load_hinge_extract(extract_b_path, 2)
        
    def _load_hinge_extract(self, file_path, user_id):
        with open(file_path, 'r') as file:
            json_data = file.read()
        name = re.search(r'HingeExtract\.(.*?)\.json', file_path).group(1)
        return HingeExtract.from_json_string(json_data, name, user_id)

    def merge(self) -> MergedExtract:
        liker = self.extract_a if self.extract_a.like else self.extract_b 
        matcher = self.extract_b if self.extract_a.match else self.extract_a
        
        users = [
            MergedUser(user_id=self.extract_a.user_id,
                       name=self.extract_a.name),
            MergedUser(user_id=self.extract_b.user_id,
                       name=self.extract_b.name),
        ]

        match = MergedMatch(
            like_user_id=liker.user_id,
            like_content=liker.like[0].content,
            like_timestamp=liker.like[0].timestamp,
            match_user_id=matcher.user_id,
            match_timestamp=matcher.match[0].timestamp
        )

        chats_a = [MergedChat(user_id=self.extract_a.user_id,
                            body=chat.body,
                            timestamp=chat.timestamp)
                            for chat in self.extract_a.chats]
        chats_b = [MergedChat(user_id=self.extract_b.user_id,
                            body=chat.body,
                            timestamp=chat.timestamp)
                            for chat in self.extract_b.chats]
        
        chats = sorted(chats_a + chats_b, key=lambda chat: chat.timestamp)
        
        result = MergedExtract(
            users=users,
            match=match,
            chats=chats
        )

        return result


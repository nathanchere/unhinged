from __future__ import annotations
from typing import List, Optional
from pydantic import BaseModel, RootModel

# NOTE: the 'type' field is redundant in each and can be ignored

class LikeItem(BaseModel):
    content: str
    timestamp: str

class BlockItem(BaseModel):
    block_type: str
    timestamp: str
    report_reason: Optional[str] = None
    report_detail: Optional[str] = None

class MatchItem(BaseModel):
    timestamp: str

class Chat(BaseModel):
    body: str
    timestamp: str

class WeMetItem(BaseModel):
    timestamp: str
    did_meet_subject: str
    was_my_type: bool

class ConnectionItem(BaseModel):
    like: Optional[List[LikeItem]] = []
    block: Optional[List[BlockItem]] = []
    match: Optional[List[MatchItem]] = []
    chats: Optional[List[Chat]] = []
    we_met: Optional[List[WeMetItem]] = []

    @staticmethod
    def from_json_string(input: str) -> ConnectionItem:
        return ConnectionItem.model_validate_json(input)

class HingeData(RootModel):
    root: List[ConnectionItem]

    @staticmethod
    def from_json_string(input: str) -> HingeData:
        return HingeData.model_validate_json(input)



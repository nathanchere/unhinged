from __future__ import annotations
from .hinge_data import Chat, ConnectionItem, LikeItem, MatchItem, WeMetItem
from pydantic import BaseModel
from typing import List, Optional

class HingeExtract(BaseModel):
    user_id: Optional[int] = 0
    name: Optional[str] = ''
    like: Optional[List[LikeItem]] = []
    match: Optional[List[MatchItem]] = []
    chats: Optional[List[Chat]] = []

    @staticmethod
    def from_json_string(input: str, name: str, user_id: int) -> ConnectionItem:
        result = HingeExtract.model_validate_json(input)
        result.user_id = user_id
        result.name = name
        return result

class MergedExtract(BaseModel):
    match: Optional[MergedMatch] = None
    chats: Optional[list[MergedChat]] = []
    users: Optional[list[MergedUser]] = []

class MergedUser(BaseModel):
    user_id: Optional[int] = 0
    name: Optional[str] = ''

class MergedMatch(BaseModel):
    like_user_id: Optional[int] = 0
    like_content: Optional[str] = ''
    match_timestamp: Optional[str] = ''
    match_user_id: Optional[int] = 0
    like_timestamp:Optional[str]=''

class MergedChat(BaseModel):
    body: Optional[str] = ''
    timestamp: Optional[str] = ''
    user_id: Optional[int] = 0

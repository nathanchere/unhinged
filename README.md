# UnHinged

![UnHinged logo](img/logo.png)

[Hinge](https://hinge.co) cites itself as "the dating app designed to be deleted".

It's a relatively noble idea, but what happens when you've found your match and it's time to delete Hinge but you don't want to lose your precious (or embarassing) memories about how you met? What do you do?

You can export your personal data but it doesn't include any personal information about anyone you interacted with, which none of the messages you received will be exported. That's where Unhinged comes to the rescue.

This is a tool to help you extract the relevant information from Hinge data exports so you can confidently delete your Hinge account while still preserving your chat history in a human-friendly format AND respecting each other's privacy in the process.

## Usage

Forewarning: I don't mind hand-holding to a point but if for example you have no idea what `git clone` means or how to run a Python script, this is not the software for you.

### How it works

Each user [exports their user data from Hinge](https://hingeapp.zendesk.com/hc/en-us/articles/360004792234-Data-Requests). You will each end up with a .7z archive of your data.

You each run the data extractor which will help you identify which chat is the one you want to back up, and it will export that chat to a new file.

You take this extracted file, combine it with ther other user's extracted file and presto. You have a tidy archive of how you met.

Additional documentation about the Hinge data export format is available in TODO:link to blog post

### Requirements

* Python >= 3.11 (might work with lower, I haven't checked)
* [Poetry](https://python-poetry.org)
* Exported Hinge data for both users

### Running

* Clone the repository somewhere locally
* Install dependencies with `poetry install` 
* Verify everything is working with `poetry run pytest`
* Save a copy of your Hinge-exported data to the `./data` directory (it should already have `example1.7z` and `example2.7z` files in there if you're looking in the right place)
* Run `poetry run main` and choose "Extract". Follow the prompts to end up with a new `HingeExtract.xxxx.json` file in your `./data` directory
* Have the other person do the same. One of you will need to send your extract json to the other person. Whoever runs the next step should have both of the extract files in their `./data` director
* Run `poetry run main` again and choose "Merge". Follow the prompts to end up wtih your merged data archive.

### Limitations

* voice notes are not currently supported. I haven't been able to get hold of any Hinge data exports that contained voice notes.

## Version history

* v1 (2024-01-06)
    - Text-based interface
    - Extract and merge data

### Roadmap

* v2

    - Import media from CDN
    - 'pretty' HTML transform

* vMaybe (if anyone else finds this useful, otherwise I don't care;)

    - GUI, pyinstall package
    - voice note support?

## The Obligatory Stuff

UnHinged is an independent software tool created by Nathan Chere. This tool is not affiliated with or endorsed by Hinge or its parent company. The use of the name "Hinge" within this software, its documentation, or any related promotional material is for descriptive purposes only.
I do not claim any ownership, sponsorship, or endorsement by Hinge, and any trademarks, registered trademarks, product names, and company names mentioned are the property of their respective owners.

UnHinged is provided "as is" without any warranty, express or implied. The use of this software is at your own risk. I accept no liability for any damages or issues arising from the use or inability to use UnHinged.
This includes, but is not limited to, any direct, indirect, special, incidental, or consequential damages or losses. I do not guarantee the accuracy, reliability, or completeness of UnHinged or its outputs. 

UnHinged is licensed under [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
